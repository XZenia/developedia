@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Create a New Project</div>
                <div class="card-body">
                    <form method="POST" action="{{ route('projects.store') }}">
                        @csrf
                        <div class="form-group">
                            <label for="name">Project Name</label>
                            <input type="text" name="name" class="form-control" required>
                        </div>

                        <div class="form-group">
                            <label for="tags">Tags</label>
                            <input type="text" data-plugin="tokenfield" name="tags" class="form-control tokenfield">
                        </div>

                        <button type="submit" class="btn btn-primary float-right">Create Project</button>
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
    $('.tokenfield').tokenfield()
</script>
@endsection
