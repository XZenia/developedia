@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <span> {{ $project->name }} entries </span>
                    <a href="{{ url()->to('/') . '/entries/create/' . $project->id }}" class="btn btn-primary btn-sm float-right">Create a new entry</a>
                </div>

                <div id="entries"/>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')

<script type="text/javascript">
var data = @json($entries, JSON_PRETTY_PRINT);
</script>

@endsection