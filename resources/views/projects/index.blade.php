@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <span> My Projects </span>
                    <a href="{{ route('projects.create') }}" class="btn btn-primary btn-sm float-right">Create a new Project</a>
                </div>
                <div id="projects"/>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')

<script type="text/javascript">
    var data = @json($projects, JSON_PRETTY_PRINT);
    console.log(data);
</script>

@endsection