@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    {{ $selectedEntry->title }}
                    <span class="float-right">
                        <a href="{{ route('entries.edit', $selectedEntry->id) }}" class="btn btn-sm btn-primary">Edit</a>
                    </span>
                </div>

                <div class="card-body">
                    {!! $selectedEntry->content !!}
                    <br>
                    Tags: {{ $selectedEntry->tags }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
