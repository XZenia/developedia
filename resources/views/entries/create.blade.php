@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Create New Entry</div>
                <div class="card-body">
                    <form method="POST" action="{{ route('entries.store') }}">
                        @csrf
                        <input type="hidden" name="project_id" value="{{$project->id}}">
                        <div class="form-group">
                            <label for="title">Title</label>
                            <input type="text" name="title" class="form-control" required>
                        </div>
                        
                        <div class="form-group template-entry">
                            <label for="template_id">Content Template</label>
                            <select class="form-control template_id">
                                <option value="">Select a Template</option>
                                @foreach ($templates as $template)
                                    <option value="{{ $template->content }}">{{ $template->title }}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="content">Content</label>
                            <textarea name="content" class="form-control entry-content"></textarea>
                        </div>

                        <div class="form-group">
                            <label for="tags">Tags</label>
                            <input type="text" data-plugin="tokenfield" name="tags" class="form-control tokenfield">
                        </div>

                        <button type="submit" class="btn btn-primary float-right">Create Entry</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
    $('.tokenfield').tokenfield();
    var simplemde = new EasyMDE();
    simplemde.value($('.template_id').val());

    $('.template_id').on('change', function(){
        simplemde.value(this.value);
    });
</script>
@endsection

