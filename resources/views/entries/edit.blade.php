@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    Edit {{ $entry->title }}
                    <span class="float-right">
                        <form method="POST" action="{{ url('entries/destroy/' . $entry->id) }}">
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="btn btn-danger btn-sm">Delete</button>
                        </form>
                    </span>
                </div>
                <div class="card-body">
                    <form method="POST" action="{{ route('entries.update', $entry->id) }}">
                        @csrf
                        @method('PUT')
                        <input type="hidden" name="project_id" value="{{$entry->id}}">
                        <div class="form-group">
                            <label for="title">Title</label>
                            <input type="text" name="title" class="form-control" value="{{ $entry->title }}" required>
                        </div>

                        <div class="form-group template-entry">
                            <label>Content Template</label>
                            <select class="form-control template_id">
                                <option id="previousContentInput" value="">Select a Template</option>
                                @foreach ($templates as $template)
                                    <option value="{{ $template->content }}">{{ $template->title }}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="content">Content</label>
                            <textarea name="content" class="form-control entry-content">{!! $entry->content !!}</textarea>
                        </div>

                        <div class="form-group">
                            <label for="tags">Tags</label>
                            <input type="text" data-plugin="tokenfield" name="tags" value="{{ $entry->tags }}" class="form-control tokenfield">
                        </div>

                        <button type="submit" class="btn btn-primary float-right">Edit Entry</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
    $('.tokenfield').tokenfield()
    var simplemde = new EasyMDE({ element: $(".entry-content")[0] });

    $('.template_id').on('change', function(){
        simplemde.value(this.value);
    });
</script>
@endsection

