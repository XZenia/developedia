@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    Edit {{ $template->title }}
                    <span class="float-right">
                        <form method="POST" action="{{ url('templates/destroy/' . $template->id) }}">
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="btn btn-danger btn-sm">Delete</button>
                        </form>
                    </span>
                </div>
                <div class="card-body">
                    <form method="POST" action="{{ route('templates.update', $template->id) }}">
                        @csrf
                        @method('PUT')
                        <input type="hidden" name="project_id" value="{{$template->id}}">
                        <div class="form-group">
                            <label for="title">Title</label>
                            <input type="text" name="title" class="form-control" value="{{ $template->title }}" required>
                        </div>
                        <div class="form-group">
                            <label for="content">Content</label>
                            <textarea name="content" class="form-control entry-content">{!! $template->content !!}</textarea>
                        </div>

                        <div class="form-group">
                            <label>Template is Exclusive to Project</label>
                            <input type="checkbox" class="ml-2" id="template-exclusive-to-project" {{ $template->project_id != '' ? 'checked' : '' }}>
                        </div>

                        <div class="form-group template-project {{ $template->project_id === '' ? 'hidden' : '' }}">
                            <label for="project_id">Project</label>
                            <select class="form-control project_id" name="project_id">
                                <option value="">Select a Project</option>
                                @foreach ($projects as $project)
                                    <option value="{{ $project->id }}" {{ $project->id == $template->project_id ? 'selected' : '' }}>{{ $project->name }}</option>
                                @endforeach
                            </select>
                        </div>

                        <button type="submit" class="btn btn-primary float-right">Edit Template</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
    $('.tokenfield').tokenfield()
    var simplemde = new EasyMDE({ element: $(".entry-content")[0] });
    $('#template-exclusive-to-project').on('click', function(){
        var hasHiddenClass = $('.template-project').hasClass('hidden');
        if (hasHiddenClass){
            $(".project_id").val("");
            $(".project_id").attr("name", "project_id");
            $('.template-project').removeClass('hidden');
        } else {
            $(".project_id").val("");
            $(".project_id").attr("name", "");
            $('.template-project').addClass('hidden');
        }
    });
</script>
@endsection

