@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <span> My Templates </span>
                    <a href="{{ route('templates.create') }}" class="btn btn-primary btn-sm float-right">Create a new Templates</a>
                </div>
                <div id="templates"/>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')

<script type="text/javascript">
    var data = @json($templates, JSON_PRETTY_PRINT);
</script>

@endsection