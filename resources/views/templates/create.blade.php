@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Create New Template</div>
                <div class="card-body">
                    <form method="POST" action="{{ route('templates.store') }}">
                        @csrf
                        <div class="form-group">
                            <label for="title">Title</label>
                            <input type="text" name="title" class="form-control" value="{{ old('title') }}" required>
                        </div>
                        <div class="form-group">
                            <label for="content">Content</label>
                            <textarea name="content" class="form-control entry-content">{{ old('content') }}</textarea>
                        </div>

                        <div class="form-group">
                            <label>Template is Exclusive to Project</label>
                            <input type="checkbox" class="ml-2" id="template-exclusive-to-project">
                        </div>

                        <div class="form-group template-project hidden">
                            <label for="project_id">Project</label>
                            <select class="form-control project_id" name="project_id">
                                <option value="">Select a Project</option>
                                @foreach ($projects as $project)
                                    <option value="{{ $project->id }}">{{ $project->name }}</option>
                                @endforeach
                            </select>
                        </div>

                        <button type="submit" class="btn btn-primary float-right">Create Template</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
    $('.tokenfield').tokenfield()
    var simplemde = new EasyMDE({ element: $(".entry-content")[0] });

    $('#template-exclusive-to-project').on('click', function(){
        var hasHiddenClass = $('.template-project').hasClass('hidden');
        if (hasHiddenClass){
            $(".project_id").val("");
            $(".project_id").attr("name", "project_id");
            $('.template-project').removeClass('hidden');
        } else {
            $(".project_id").val("");
            $(".project_id").attr("name", "");
            $('.template-project').addClass('hidden');
        }
    });
</script>
@endsection

