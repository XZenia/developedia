import React from 'react';
import ReactDOM from 'react-dom';
import EditablePage from './editor/EditablePage';

class CreateEntry extends React.Component{
    constructor(props) {
        super(props);
        this.state = { blocks: [initialBlock] };
    }

    render(){
        return (
            <EditablePage></EditablePage>
        );
    }
}

export default CreateEntry;

if (document.getElementById('create-entry')) {
    ReactDOM.render(<CreateEntry />, document.getElementById('create-entry'));
}

