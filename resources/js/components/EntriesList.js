import React from 'react';
import ReactDOM from 'react-dom';

class EntriesList extends React.Component {
    render(){
        if (data && data.length > 0){
            return (
                <div className="card-body">
                    {data.map(entry => (
                        <ul className="list-group list-group-flush" key={entry.id}>
                            <li className="list-group-item">
                                <a href={baseUrl + "entries/" + entry.id}> { entry.title } </a>
                            </li>
                        </ul>
                    ))}
                </div>
            );
        } else {
            return (
                <div className="card-body">
                    <h4> No entries available! Add one by clicking the "Add Entries" button. </h4>
                </div>
            );
        }

    }
}
export default EntriesList;

if (document.getElementById('entries')) {
    ReactDOM.render(<EntriesList />, document.getElementById('entries'));
}
