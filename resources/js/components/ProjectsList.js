import React from 'react';
import ReactDOM from 'react-dom';

class ProjectsList extends React.Component {
    render(){
        if (data && data.length > 0){
            return (
                <div className="card-body">
                    {data.map(project => (
                        <ul className="list-group list-group-flush"  key={project.id}>
                            <li className="list-group-item d-flex justify-content-between align-items-center">
                                <a href={baseUrl + "projects/" + project.id}> { project.name } </a>
                                <span className="badge badge-primary badge-pill">{ project.numberOfProjects }</span>
                            </li>
                        </ul>
                    ))}
                </div>
            );
        } else {
            return (
                <div className="card-body">
                    <h4> No projects available! Add one by clicking the "Add Project" button. </h4>
                </div>
            );
        }
    }
}

export default ProjectsList;

if (document.getElementById('projects')) {
    ReactDOM.render(<ProjectsList />, document.getElementById('projects'));
}
