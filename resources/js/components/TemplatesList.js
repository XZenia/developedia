import React from 'react';
import ReactDOM from 'react-dom';

class TemplatesList extends React.Component {
    render(){
        if (data && data.length > 0){
            return (
                <div className="card-body">
                    {data.map(templates => (
                        <ul className="list-group list-group-flush"  key={templates.id}>
                            <li className="list-group-item d-flex justify-content-between align-items-center">
                                <a href={baseUrl + "templates/" + templates.id + "/edit"}> { templates.title } </a>
                            </li>
                        </ul>
                    ))}
                </div>
            );
        } else {
            return (
                <div className="card-body">
                    <h4> No templates available! Add one by clicking the "Add Templates" button. </h4>
                </div>
            );
        }
    }
}

export default TemplatesList;

if (document.getElementById('templates')) {
    ReactDOM.render(<TemplatesList />, document.getElementById('templates'));
}
