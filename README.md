# Developedia

Developedia is a work in progress web app that aims to make documentation of software development projects easier by having a centralized body of knowledge similar to a wiki. This is done through markdown editors within the web application and an API that paves the way for VSCode integration and other applications such as mobile apps that allow easy viewing of entries.

## Prerequisites

1. Composer
2. NPM

## Setting it up in your local environment

1. Once you have the prerequisites installed and you have cloned the project, go to the folder where the project files are and open up a terminal. Type "composer install"
2. Once that is done, type in "npm install" to set up React and other front end dependencies.
3. If you have not added a MySQL table called "developedia", do so now. Once done, type in "php artisan migrate:fresh".
4. Create another table called "developedia_test" for testing. You can opt to store testing data through other means such as sqlite or in memory but that is not the scope of this project.
5. In order to run the tests, type in "php artisan test"
6. Have fun.

## Stuff I plan to add in the future
1. Login and Registration
2. Project access can be configured on a user by user basis.
3. VSCode Integration
4. Rainbows and unicorns idk