<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Models\Template;
use App\Models\Project;

use App\Http\Requests\TemplateRequest;

use Sinnbeck\Markdom\Facades\Markdom;

class TemplateController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $templates = Template::where('user', auth()->user()->id)->get();
        return view('templates.index', compact('templates'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $projects = Project::where('user', auth()->user()->id)->get();
        return view('templates.create', compact('projects'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TemplateRequest $request)
    {
        if (isset($template) && $template->project == auth()->user()->id){
            $newTemplate = Template::create($request->all());
        }
        return redirect()->route('templates.show', $newTemplate->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $template = Template::find($id);
        if (isset($template) && $template->user == auth()->user()->id){
            $projects = Project::where('user', auth()->user()->id)->get();
            return view('templates.edit', compact('template', 'projects'));
        } else {
            return redirect()->back()->withErrors("Template does not exist.");
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $template = Template::find($id);
        $projects = Project::where('user', auth()->user()->id)->get();
        if (isset($template)){
            return view('templates.edit', compact('template', 'projects'));
        } else {
            return redirect()->back()->withErrors("Template does not exist.");
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(TemplateRequest $request, $id)
    {
        $selectedTemplate = Template::find($id);
        if (isset($selectedTemplate)){
            if($selectedTemplate->user == auth()->user()->id){
                $selectedTemplate->update($request->all());
                return redirect()->route('templates.show', $selectedTemplate->id);
            } else {
                return redirect()->back()->withError("The template you are trying to edit is invalid. Please create a new one and try again.");
            }
        } else {
            return redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $selectedTemplate = Template::find($id);

        if (isset($selectedTemplate)){
            if($selectedTemplate->user == auth()->user()->id){
                $selectedTemplate->delete();
            } else {
                return redirect()->back()->withError("The template you are trying to delete is invalid.");
            }
        }

        return redirect()->route('templates.index');
    }
}
