<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Models\Template;
use App\Models\Project;

use App\Http\Requests\TemplateRequest;

use Sinnbeck\Markdom\Facades\Markdom;

class TemplateController extends Controller
{
    public function getProjectTemplates($projectId)
    {
        $project = Project::find($projectId);
        $templates = [];
        if (isset($project)){
            $templates = Template::where('project_id', $project->id)->orWhereNull('project_id')->get();
        }

        return $templates;
    }
}
