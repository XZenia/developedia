<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Models\Entry;
use App\Models\Project;
use App\Models\Template;
use App\Http\Requests\EntryRequest;

use Sinnbeck\Markdom\Facades\Markdom;

class EntryController extends Controller
{
    public function getEntries($projectId)
    {
        $project = Project::find($projectId);
        $entries = [];
        if (isset($project)){
            $entries = Entry::where('project_id', $project->id)->get();
        }

        return $entries;
    }

    public function store(EntryRequest $request)
    {
        $newEntry = Entry::create($request->all());
        return response()->json(['entry' => $newEntry], Response::HTTP_CREATED);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $selectedEntry = Entry::find($id);
        if (isset($selectedEntry)){
            $selectedEntry->content = Markdom::toHtml($selectedEntry->content);
            return response()->json(['entry' => $selectedEntry], Response::HTTP_OK);
        } else {
            return response()->json(['error' => "Entry not found."], Response::HTTP_NOT_FOUND);
        }
    }

    public function update(EntryRequest $request, $id)
    {
        $selectedEntry = Entry::find($id);
        if (isset($selectedEntry)){
            $selectedEntry->update($request->all());
            return response()->json(['entry' => $selectedEntry], Response::HTTP_OK);
        } else {
            return response()->json(['error' => "Entry not found."], Response::HTTP_NOT_FOUND);
        }
    }

    public function destroy($id)
    {
        $selectedEntry = Entry::find($id);

        if (isset($selectedEntry)){
            $selectedEntry->delete();
            return response()->json(['error' => "Entry successfully deleted."], Response::HTTP_NO_CONTENT);
        } else {
            return response()->json(['error' => "Entry not found."], Response::HTTP_NOT_FOUND);
        }
    }
}
