<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Models\Project;
use App\Models\Entry;

use App\Http\Requests\ProjectRequest;

class ProjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getAllProjects()
    {
        $projects = Project::all();

        foreach($projects as $project){
            $project->numberOfProjects = Entry::where('project_id', $project->id)->count();
        }

        return response()->json($projects);
    }
}
