<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Models\Entry;
use App\Models\Project;
use App\Models\Template;
use App\Http\Requests\EntryRequest;

use Sinnbeck\Markdom\Facades\Markdom;

class EntryController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($projectId)
    {
        $project = Project::find($projectId);
        $templates = Template::where('project_id', $project->id)->orWhereNull('project_id')->get();
        if (isset($project)){
            return view('entries.create', compact('project', 'templates'));
        } else {
            return redirect()->back();
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(EntryRequest $request)
    {
        $newEntry = Entry::create($request->all());
        return redirect()->route('entries.show', $newEntry->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $selectedEntry = Entry::find($id);
        if (isset($selectedEntry)){
            $selectedEntry->content = Markdom::toHtml($selectedEntry->content);
            return view('entries.view', compact('selectedEntry'));
        } else {
            return redirect()->route('projects.index');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $entry = Entry::find($id);
        $templates = Template::where('project_id', $entry->project_id)->orWhereNull('project_id')->get();
        if (isset($entry)){
            return view('entries.edit', compact('entry', 'templates'));
        } else {
            return redirect()->back();
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(EntryRequest $request, $id)
    {
        $selectedEntry = Entry::find($id);
        if (isset($selectedEntry)){
            $selectedEntry->update($request->all());
            return redirect()->route('entries.show', $selectedEntry->id);
        } else {
            return redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $selectedEntry = Entry::find($id);
        $entryProjectId = $selectedEntry->project_id;

        if (isset($selectedEntry)){
            $selectedEntry->delete();
        }

        return redirect()->route('projects.show', $entryProjectId);
    }
}
