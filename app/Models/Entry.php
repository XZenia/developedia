<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Entry extends Model
{
    protected $table = 'entries';
    
    protected $primaryKey = 'id';
    
    protected $guarded = ['id'];

    public $timestamps = true;

    public function setContentAttribute($value){
        $this->attributes['content'] = $value;
    }

    public function setTagsAttribute($value){
        $this->attributes['tags'] = json_encode($value);
    }

    public function getTagsAttribute(){
        return json_decode($this->attributes['tags']);
    }
}
