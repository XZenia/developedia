<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Project extends Model
{
    use HasFactory;

    protected $table = 'projects';
    
    protected $primaryKey = 'id';
    
    protected $guarded = ['id'];

    /**
     * The "booted" method of the model.
     *
     * @return void
     */
    protected static function booted()
    {
        static::creating(function ($project) {
            $project->user = auth()->user()->id;
        });
    }

    public function setTagsAttribute($value){
        $this->attributes['tags'] = json_encode($value);
    }

    public function getTagsAttribute(){
        return json_decode($this->attributes['tags']);
    }

    public $timestamps = true;
}
