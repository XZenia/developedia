<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Template extends Model
{
    protected $table = 'templates';
    
    protected $primaryKey = 'id';
    
    protected $guarded = ['id'];

    public $timestamps = true;

    /**
     * The "booted" method of the model.
     *
     * @return void
     */
    protected static function booted()
    {
        static::creating(function ($template) {
            $template->user = auth()->user()->id;
        });
    }

    public function setContentAttribute($value){
        $this->attributes['content'] = $value;
    }
}
