<?php

namespace Tests\Feature;

use Illuminate\Http\Response;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

use App\Models\Project;
use App\Models\Entry;

class EntryControllerTest extends TestCase
{
    use RefreshDatabase;

    public function test_saves_a_new_entry()
    {
        $testProject = Project::factory()->create();

        $response = $this->post(route('entries.store'), [
            'project_id' => $testProject->id,
            'title' => 'Test!',
            'content' => 'Test content!',
        ]);

        $response->assertStatus(Response::HTTP_FOUND);
    }


    public function test_can_view_an_entry()
    {
        $testProject = Project::factory()->create();

        $testEntry = Entry::create([
            'project_id' => $testProject->id,
            'title' => 'Test!',
            'content' => 'Test content!',
        ]);
        $response = $this->get(route('entries.show', $testEntry->id));

        $response->assertStatus(Response::HTTP_OK);
    }

    public function test_invalid_entry_goes_to_404_error()
    {
        $response = $this->get(route('entries.show', 9999999));

        $response->assertStatus(Response::HTTP_FOUND);
    }

    public function test_can_update_an_entry()
    {
        $testProject = Project::factory()->create();

        $testEntry = Entry::create([
            'project_id' => $testProject->id,
            'title' => 'Test!',
            'content' => 'Test content!',
        ]);

        $response = $this->patch(route('entries.update', $testEntry->id), [
            'project_id' => $testProject->id,
            'title' => 'Updated title!',
            'content' => 'Updated content!',
        ]);

        $response->assertRedirect(route('entries.show', $testEntry->id));
    }

    public function test_can_delete_an_entry()
    {
        $testProject = Project::factory()->create();

        $testEntry = Entry::create([
            'project_id' => $testProject->id,
            'title' => 'Test!',
            'content' => 'Test content!',
        ]);

        $response = $this->delete(route('entries.destroy', $testEntry->id));
        
        $testQuery = Entry::find($testEntry->id);
        $this->assertNull($testQuery);
    }
}
