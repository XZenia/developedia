<?php

namespace Tests\Feature;

use Illuminate\Http\Response;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

use App\Models\Project;
use App\Models\Template;

class TemplateControllerTest extends TestCase
{
    use RefreshDatabase;

    public function test_saves_a_new_template_with_project_id()
    {
        $testProject = Project::factory()->create();

        $response = $this->post(route('templates.store'), [
            'project_id' => $testProject->id,
            'title' => 'Test!',
            'content' => 'Test content!',
        ]);

        $response->assertStatus(Response::HTTP_FOUND);
    }

    public function test_saves_a_new_template_without_project_id()
    {
        $testProject = Project::factory()->create();

        $response = $this->post(route('templates.store'), [
            'title' => 'Test!',
            'content' => 'Test content!',
        ]);

        $response->assertStatus(Response::HTTP_FOUND);
    }


    public function test_can_view_an_template()
    {
        $testProject = Project::factory()->create();
        
        $testTemplate = Template::create([
            'project_id' => $testProject->id,
            'title' => 'Test!',
            'content' => 'Test content!',
        ]);
        $response = $this->get(route('templates.show', $testTemplate->id));

        $response->assertStatus(Response::HTTP_OK);
    }

    public function test_invalid_template_goes_to_404_error()
    {
        $response = $this->get(route('templates.show', 9999999));

        $response->assertStatus(Response::HTTP_FOUND);
    }


    public function test_can_update_an_template()
    {
        $testProject = Project::factory()->create();

        $testTemplate = Template::create([
            'project_id' => $testProject->id,
            'title' => 'Test!',
            'content' => 'Test content!',
        ]);

        $response = $this->patch(route('templates.update', $testTemplate->id), [
            'title' => 'Updated title!',
            'content' => 'Updated content!',
        ]);

        $response->assertRedirect(route('templates.show', $testTemplate->id));
    }

    public function test_can_delete_an_template()
    {
        $testProject = Project::factory()->create();

        $testTemplate = Template::create([
            'project_id' => $testProject->id,
            'title' => 'Test!',
            'content' => 'Test content!',
        ]);

        $response = $this->delete(route('templates.destroy', $testTemplate->id));

        $response->assertRedirect(route('templates.index'));
    }
}
