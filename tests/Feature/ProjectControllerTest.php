<?php

namespace Tests\Feature;

use Illuminate\Http\Response;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

use App\Models\Project;

class ProjectControllerTest extends TestCase
{
    use RefreshDatabase;

    public function test_saves_a_new_project()
    {
        $response = $this->post(route('projects.store'), [
            'name' => 'Test Project!'
        ]);

        $response->assertStatus(Response::HTTP_FOUND);
    }

    public function test_invalid_project_goes_to_404_error()
    {
        $response = $this->get(route('projects.show', 9999999));

        $response->assertStatus(Response::HTTP_NOT_FOUND);
    }



    public function test_can_view_a_project()
    {
        $testProject = Project::factory()->create();

        $response = $this->get(route('projects.show', $testProject->id));

        $response->assertStatus(Response::HTTP_OK);
    }
}
