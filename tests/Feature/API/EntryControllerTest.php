<?php

namespace Tests\Feature\API;

use Illuminate\Http\Response;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

use App\Models\Project;
use App\Models\Entry;

class EntryControllerTest extends TestCase
{
    use RefreshDatabase;

    public function test_saves_a_new_entry()
    {
        $testProject = Project::factory()->create();

        $response = $this->json('post', 'api/entries', [
            'project_id' => $testProject->id,
            'title' => 'Test!',
            'content' => 'Test content!',
        ])->assertStatus(Response::HTTP_CREATED);
    }


    public function test_can_view_an_entry()
    {
        $testProject = Project::factory()->create();

        $testEntry = Entry::create([
            'project_id' => $testProject->id,
            'title' => 'Test!',
            'content' => 'Test content!',
        ]);

        $response = $this->json('get', 'api/entries/' . $testEntry->id)->assertStatus(Response::HTTP_OK);
    }

    public function test_can_update_an_entry()
    {
        $testProject = Project::factory()->create();

        $testEntry = Entry::create([
            'project_id' => $testProject->id,
            'title' => 'Test!',
            'content' => 'Test content!',
        ]);

        $response = $this->json('put', 'api/entries/' . $testEntry->id, [
            'project_id' => $testProject->id,
            'title' => 'Updated title!',
            'content' => 'Updated content!',
        ])->assertStatus(Response::HTTP_OK);
    }

    public function test_cannot_update_missing_entry()
    {
        $testProject = Project::factory()->create();
        
        $response = $this->json('put', 'api/entries/0', [
            'project_id' => $testProject->id,
            'title' => 'Updated title!',
            'content' => 'Updated content!',
        ])->assertStatus(Response::HTTP_NOT_FOUND);
    }

    public function test_can_delete_an_entry()
    {
        $testProject = Project::factory()->create();

        $data = [
            'project_id' => $testProject->id,
            'title' => 'Test!',
            'content' => 'Test content!',
        ];

        $testEntry = Entry::create($data);
        $this->json('delete', 'api/entries/' . $testEntry->id);

        $testQuery = Entry::find($testEntry->id);
        $this->assertNull($testQuery);
    }
}
