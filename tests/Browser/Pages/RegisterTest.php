<?php

namespace Tests\Browser\Pages;

use App\Models\User;
use Faker\Factory;
use Laravel\Dusk\Browser;
use Tests\DuskTestCase;


class RegisterTest extends DuskTestCase
{
    /**
     * Test if application can register accounts
     *
     * @return void
     */
    public function test_can_register_account()
    {
        $faker = Factory::create();
        $this->browse(function (Browser $browser) use ($faker) {
            $browser->visit('/register')
                    ->type('name', $faker->name)
                    ->type('email', $faker->unique()->safeEmail)
                    ->type('password', 'password')
                    ->type('password_confirmation', 'password')
                    ->click('button[type="submit"]')
                    ->assertSee('Login')
                    ->assertPathIs('/login')
                    ->assertNotPresent('.is-invalid');
        });
    }

    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function test_different_passwords_are_not_accepted()
    {
        $faker = Factory::create();
        $this->browse(function (Browser $browser) use ($faker) {
            $browser->visit('/register')
                    ->type('name', $faker->name)
                    ->type('email', $faker->unique()->safeEmail)
                    ->type('password', 'password')
                    ->type('password_confirmation', 'password123')
                    ->click('button[type="submit"]')
                    ->assertSee('The password confirmation does not match.')
                    ->assertPresent('.is-invalid');
        });
    }

    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function test_same_emails_are_not_accepted()
    {
        $faker = Factory::create();
        $user = User::factory(1)->create()[0];
        $this->browse(function (Browser $browser) use ($faker, $user) {
            $browser->visit('/register')
                    ->type('name', $faker->name)
                    ->type('email', $user->email)
                    ->type('password', 'password')
                    ->type('password_confirmation', 'password')
                    ->click('button[type="submit"]')
                    ->assertSee('The email has already been taken.')
                    ->assertPresent('.is-invalid');
        });
    }
}
