<?php

namespace Tests\Browser\Pages;

use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Laravel\Dusk\Browser;
use Tests\DuskTestCase;

class LoginTest extends DuskTestCase
{
    /**
     * People can log in to the website
     *
     * @return void
     */
    public function test_can_login_account()
    {
        $user = User::factory(1)->create()[0];

        $this->browse(function (Browser $browser) use ($user) {
            $browser->visit('/login')
                    ->type('email', $user->email)
                    ->type('password', 'password')
                    ->click('button[type="submit"]')
                    ->assertSee('My Projects')
                    ->assertPathIs('/projects');
        });
    }
}
